import Link from "next/link";
import { IconType } from "react-icons";
import { twMerge } from "tailwind-merge";

interface SidebarItemProps {
  icon: IconType;
  label: string;
  active?: boolean;
  href: string;
}

// here, icon is a prop being passed in from Sidebar.tsx
// We pass in a react-icon for the icon prop
// Typically, to use a react-icon, you treat like a component and call it by its name <HiHome />
// icon is storing HiHome
// We destructure the object props and use renaming syntax of icon and rename it to Icon because react requires all components to start with a capital letter
// <HiHome /> is now represented as <Icon /> in the SidebarItem component
const SidebarItem: React.FC<SidebarItemProps> = ({
  icon: Icon,
  label,
  active,
  href,
}) => {
  return (
    <Link
      href={href}
      className={twMerge(
        `flex flex-row h-auto items-center w-full gap-x-4 text-md font-medium cursor-pointer hover:text-white transition text-neutral-400 py-1`,
        active && "text-white"
      )}
    >
      <Icon size={26} />
      <p className="truncate w-full">{label}</p>
    </Link>
  );
};

export default SidebarItem;
